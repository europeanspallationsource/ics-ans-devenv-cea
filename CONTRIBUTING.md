How to Contribute to Playbook
=============================

The best way to contribute is to be registered on DRF Gitlab. **If you are not a CEA agent** you can send suggestions or code by email to [Jean-François Denis](mailto:jfdenis@cea.fr) or to [Nicolas Senaud](mailto:nicolas.senaud@cea.fr).

## Coding Style

* Do not hard-write things subject to change (variables), use vars ond group_vars in such cases.
* Avoid warnings: do not use deprecated features or `shell` module when a function is available.

## Development Procedure Using Gitlab

### 1. Open a ticket

On Gitlab, go to the *Issues* tab, and check a ticket related to your need is not opened yet, if not open a new one, and in both cases attribute the ticket to yourself.

### 2. Create a new branch

On the ticket page, you will find a "Create branch" button. This will create a new branch named after the ticket title, and prefixed by the ticket number. You will use this branch to code the new feature. You can now clone the repository if you don't already get it, and go to the folder on your computer.

To checkout the new branch, simply use this two commands:

```bash
git pull                       # Synchronize your local git repository with Gitlab.
git checkout <new_branch_name> # Checkout the branch created by Gitlab.
```

*Tip:* If you already have the repository locally before Gitlab create the branch, `git pull` output will tell you the name of the new branch.

### 3. Code!

Add your new feature! Please, refer to the **Coding Style** section to use the right conventions.

Please, *commit and push* often! This will make it easy to recover if something bad happen on your computer, and if you modify a lot of code it will make it easier to discard some changes compared with a big single commit.

### 4. Test it!

Before submit any code, test-it! A `Vagrantfile` is available in the repository to make it easy to provision a Vagrant Box. You will need to install Vagrant and VirtualBox on your computer (on Windows you will also need to install OpenSSH, please refer to Vagrant website).

Please, make sure your code is compatible with the requirements written on the README page, or update it if necessary.

### 5. Create a Merge Request

Finally, you will need to create a merge request to ask for integration of your changes in the main development branch (`master`), and then to a future release. Your changes will be reviewed by someone listed at the top of this file before being accepted.

If you followed this guide carefully, it should not be a problem, in other case some changes might be necessary before the merge can be accepted.
