Ansible Playbook for ESS Project Workstations
=============================================

Setup a workstation to use with the ESS Epics Environment (EEE).

# What's new?

See CHANGELOG file.

# Where and when to use this playbook?

This playbook must be used to setup ESS development workstations at CEA and at INFN Catania, and it might be adapted to be used elsewhere with EEE.

# Requirements

This playbook is intendeed to be used on PCs following the ESS project recommandations, which currently are:

* A x86 64 bits processor – it might be a victual machine
* CentOS 7.1 (be carefull, this is not the last release of CentOS 7)
* `git` and `ansible` installed via Epel repository
* Gnome 3 graphical environment

# How to use it?

There is currently two scenario:

* If you need to setup a development machine inside CEA for the ESS project, you should not use this playbook by yourself. Instead, contact [Jean-François Denis](mailto:jfdenis@cea.fr) or [Nicolas Senaud](mailto:nicolas.senaud@cea.fr), so that your PC will be automatically updated.
* If you need to setup a development machine for INFN Catania, you only need to run `./update.sh catania` and the script will do all the setup automatically. Note that you are supposed to have a registered machine on the INFN network.
* In every other cases, this playbook is probably not for you, but feel free to adapt it to your needs.

# How to contribute?

See CONTRIBUTING.md file.
