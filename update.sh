#! /usr/bin/env bash

if [[ $1 != "" ]]; then
    if [[ $2 != "" ]]; then
        ansible-playbook --ask-vault-pass -i hosts -u root -f 8 -l $1 devenv.yml --tags $2 || exit $?
    else
        ansible-playbook --ask-vault-pass -i hosts -u root -f 8 -l $1 devenv.yml || exit $?
    fi
else
    echo -e "Usage: ./update <subset>\nExample: ./update tests"
fi
